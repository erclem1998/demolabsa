const { Usuario } = require('../models/user.model')
const { Evento } = require('../models/event.model')

const getEvents = async( uid ) => {
    try {
        Usuario.hasMany(Evento, {
            foreignKey: 'uid',
            sourceKey: 'uid',
        })
        Evento.belongsTo(Usuario, {
            foreignKey: 'uid',
            sourceKey: 'uid',
        })
        const eventList = await Evento.findAll({
            where: {
                uid
            }
        })
        // console.log(eventList)
        return eventList

    } catch (error) {
        console.log(error)
    }
}

const createEvent = async (title = '', notes = '', startDate = '', endingDate = '', uid) => {
    try {
        const data = {
            notes,
            startDate,
            endingDate,
            title,
            uid
        }
        const evento = new Evento({...data})
        const newEvent = await(await evento.save()).reload();

        return {
            ok: true,
            user: {
                ...newEvent
            }
        }

    } catch (error) {
        
    }
}

module.exports = {
    getEvents,
    createEvent
}