// const path = require('path')
// const dotenv = require('dotenv');
// dotenv.config({path: path.resolve(__dirname, '../../.env/.env')})
const { generateJWT } = require('../../helpers/generate-jwt');
require('jasmine')

describe('Pruebas para generar un jwt', () => {
    it('Debería generar correctamente wl jwt', async() => {
        // Arrange
        const userData = {
            uid: 1,
            name: 'Erick Lemus',
            email: 'erick-email@fake.com'
        }
        // Act
        const jwt = await generateJWT({...userData})
        // Assert
        expect(jwt).not.toBeNull()
        expect(jwt.length).toBeGreaterThan(0);
    });
});