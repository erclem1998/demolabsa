'use strict'
require('dotenv').config({ path:__dirname + '/.env/.env' });
const express = require('express');
const cors = require('cors')

const {PORT} = process.env;

// CREAR EL SERVIDOR
const app = express();

// CORS
app.use(cors())

// PARSEAR DEL BODY
app.use(express.json())

app.use('/api/example', require('./routes/example.route'))

app.listen(PORT, () => {
    console.log(`Servidor corriendo en puerto ${PORT}`)
})