const { suma, resta } = require('../../helpers/myFunctions')

// LAS PRUEBAS SE EJECUTAN O SSE CONSTRUYEN EN BASE A LAS 3A DE LAS PRUEBAS
// ARRANGE: SE REFIERE A LA PREPARACIÓN DE LOS DATOS DE PRUEBA
// ACT: SE REFIERE A LA EJECUCIÓN DEL ELEMENTO OBJETIVO
// ASSERT: QUE VERIFICA SSI ES RESULTADO ES EL ESPERADO

describe('Prueba sobre la funcion de suma', () => { 
    it('Deberia devolver la suma = 30', () => {
        //arrange
        const a = 10;
        const b = 20;
        // act
        const result = suma(a,b);
        //assert
        expect(result).toBe(30);
    })
})

describe('Prueba sobre la funcion de restta', () => { 
    it('Deberia devolver la resta = -10', () => {
        //arrange
        const a = 10;
        const b = 20;
        // act
        const result = resta(a,b);
        //assert
        expect(result).toBe(-10);
    })
})