const { Router } = require('express')
const { getSuma, getResta } = require('../controller/example.controller')

const router = Router();

router.post('/suma', getSuma); // localhost:3000/api/example/suma
router.post('/resta', getResta); // localhost:3000/api/example/resta

module.exports = router;
