const amqp = require('amqplib');
var channel, connection;


async function connectToRabbitMQ() {
  const amqpServer = "amqp://myuser:mypassword@localhost:5672";
  connection = await amqp.connect(amqpServer);
  channel = await connection.createChannel();
  await channel.assertQueue('new_user_queue', { durable: true });
  try {
    console.log('Esperando nuevos usuarios')
    await channel.consume('new_user_queue', (data) => {
        // console.log({"mensaje_recibido": data.content.toString()})
        const nuevoUsuario = JSON.parse(data.content);
        console.log({recibido: `Usuario ${nuevoUsuario.dataNewUser.username} recibido`})
        channel.ack(data);
        channel.sendToQueue('response_queue', Buffer.from(JSON.stringify({
            msg: `Usuario ${nuevoUsuario.dataNewUser.username} creado`,
            ok: true
        })))
    });
    
  } catch (error) {
    console.error('Error en el consumidor de usuarios nuevo:', error);
  }
}

connectToRabbitMQ();