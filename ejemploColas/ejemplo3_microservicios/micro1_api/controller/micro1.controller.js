const { response } = require('express');
const amqp = require("amqplib");

let channel, connection;
let responseCallback; // Callback para manejar la respuesta

async function connectToRabbitMQ() {
    const amqpServer = "amqp://myuser:mypassword@localhost:5672";
    connection = await amqp.connect(amqpServer);
    channel = await connection.createChannel();
    await channel.assertQueue("response_queue");

    // Nos subscribimos a la cola response_data una sola vez
    await channel.consume("response_queue", (data) => {
        const respuesta = JSON.parse(data.content);
        console.log({ respuesta });
        channel.ack(data);

        // Si hay un callback registrado, lo llamamos con la respuesta
        if (responseCallback) {
            responseCallback(respuesta);
        }
    });
}
connectToRabbitMQ();

const newUserController = async (req, res = response) => {
    const { 
        name,
        username,
        passwd,
        email
    } = req.body;

    if (name != null && username != null && passwd != null && email != null) {
        // Enviamos el mensaje a la cola enviar_data
        channel.sendToQueue(
            "new_user_queue",
            Buffer.from(JSON.stringify({ dataNewUser: { ...req.body } }))
        );
        // Configuramos el callback para manejar la respuesta
        responseCallback = (respuesta) => {
            res.status(200).send({
                ok: true,
                ...respuesta
            });
            // Limpiamos el callback después de usarlo
            responseCallback = null;
        };
    } else {
        res.status(400).send({
            ok: false,
            msg: `Formulario inválido`
        });
    }
}

module.exports = {
    newUserController,
};
